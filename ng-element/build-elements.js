const fs = require('fs-extra');
const concat = require('concat');

(async function build() {

    const es2015Files = [
        './dist/ng-element/runtime-es2015.js',
        './dist/ng-element/polyfills-es2015.js',
        './dist/ng-element/main-es2015.js'
    ];
    const es5Files = [
        './dist/ng-element/runtime-es5.js',
        './dist/ng-element/polyfills-es5.js',
        './dist/ng-element/main-es5.js'
    ];
    await fs.ensureDir('lib');
    await concat(es2015Files, './lib/ng-element-es2015.js');
    await concat(es5Files, './lib/ng-element-es5.js');


    //Copy it to using with angular folder
    await concat(es2015Files, '../using-ng-element/angular/src/assets/ng-element-es2015.js');
    await concat(es5Files, '../using-ng-element/angular/src/assets/ng-element-es5.js');

    //Copy it to using with vue folder
    await concat(es2015Files, '../using-ng-element/vue/public/ng-element-es2015.js');
    await concat(es5Files, '../using-ng-element/vue/public/ng-element-es5.js');

    console.info('NG Element created successfully!');
})();
